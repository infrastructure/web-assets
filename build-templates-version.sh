#!/bin/bash

# Read version from file VERSION
VERSION=$(cat VERSION)

# Loop through templates
for FILE in src/templates/*.{html,pug}; do
  if [[ $FILE == *.html ]]; then
    COMMENT="<!-- @build Blender Web Assets v${VERSION} - Do not remove comment -->"
  elif [[ $FILE == *.pug ]]; then
    COMMENT="//- @build Blender Web Assets v${VERSION} - Do not remove comment"
  fi

  # Read file content
  CONTENT=$(cat "$FILE")

  # Check if first line contains "@build"
  FIRST_LINE=$(head -n 1 "$FILE")
  if [[ $FIRST_LINE == *"@build"* ]]; then
    # Replace version notice first line
    echo -e "${COMMENT}\n$(tail -n +2 "$FILE")" > "$FILE"
  else
    # Prepend version notice to file content
    echo -e "${COMMENT}\n${CONTENT}" > "$FILE"
  fi
done

# Log message
echo "Build templates complete."
