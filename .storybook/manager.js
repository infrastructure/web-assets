import { addons } from '@storybook/manager-api';
import  blenderTheme from './BlenderTheme';

import '../src/styles/_helper-storybook-fonts.css';
import './manager-styles.css';

addons.setConfig({
  theme:  blenderTheme,
});
