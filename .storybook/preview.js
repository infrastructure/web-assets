import { themes } from '@storybook/theming';
import { useDarkMode } from 'storybook-dark-mode';

import '../src/styles/_helper-storybook.sass'; // Imports styles global Blender Web Assets main
import './preview-styles.css';

/** @type { import('@storybook/html').Preview } */
const attributeDataTheme = (story) => {
  const isDark = useDarkMode();
  const storybookDocs = document.querySelector('#storybook-docs');
  const storybookDocsItem = storybookDocs.querySelectorAll('.sbdocs.sbdocs-preview');
  const storybookHTML = document.querySelector('html');
  const storybookRoot = document.querySelector('#storybook-root');

  const setAttributeDataTheme = (item) => {
    if (isDark) {
      item.setAttribute('data-theme', 'dark');
    } else {
      item.removeAttribute('data-theme');
    }
  };

  storybookDocsItem.forEach(item => {
    setAttributeDataTheme(item);
  });

  setAttributeDataTheme(storybookRoot);

  /**
   * Conditional is needed to let BWA manage the main preview container's
   * background colour in standalone stories.
   */

  if (storybookRoot.getAttribute('hidden') === 'true') {
    storybookHTML.removeAttribute('data-theme');

    storybookHTML.style.setProperty(
      '--preview-bg-color', '--preview-bg-color'
    );
  } else {
    setAttributeDataTheme(storybookHTML);

    storybookHTML.style.setProperty(
      '--preview-bg-color', 'var(--color-bg)'
    );
  }

  return story();
};

/**
 * Conditionals decoratorScriptIsLoaded is needed to prevent loadScript
 * decorators from running multiple times by triggering preview.js when
 * modifying the DOM.
 */
let decoratorScriptIsLoaded = false;

const initScriptBWA = () => {
  navbar.initDropdown();
};

const loadScript = (story) => {
  const hostname = window.location.hostname;
  let scriptPath;

  if (!decoratorScriptIsLoaded) {
    /* Set scriptPath conditionally for development and production */
    if (hostname === 'localhost') {
      scriptPath = '../src/scripts/tutti/10_navbar.js';
    } else {
      scriptPath = './10_navbar.js';
    }

    const appendScript = (src, callback) => {
      const script = document.createElement('script');

      script.onload = callback;
      script.src = src;

      document.body.appendChild(script);
    };

    appendScript(scriptPath, initScriptBWA);

    decoratorScriptIsLoaded = true;
  }

  return story();
}

/**
 * Function reInitScript is needed to reinitialize the script on Story
 * changes. Function loadScript is only initialized once with Storybook and may
 * or may not assign variables based on what story is being viewed.
 */
const reInitScript = (story) => {
  setTimeout(() => {
    initScriptBWA();
  }, 200);

  return story();
}

const preview = {
  decorators: [attributeDataTheme, loadScript, reInitScript],
  parameters: {
    backgrounds: {
      default: 'Default',
      grid: {
        cellAmount: 2,
        cellSize: 16,
        offsetX: 8, // Default is 0 if story has 'fullscreen' layout, 8 if layout is 'padded'
        offsetY: 8, // Default is 0 if story has 'fullscreen' layout, 8 if layout is 'padded'
        opacity: 0.2,
      },
      values: [
        /**
         * Enable items 'Dark' and 'Light' for explicit Storybook preview
         * background color overrides. It is on by default.
         * { name: 'Dark', value: 'hsl(0, 0%, 2%)' }, // --preview-bg-color
         * { name: 'Light', value: 'hsl(0, 0%, 98%)' }, // --preview-bg-color
         */
        { name: 'Default', value: 'transparent' },
      ],
    },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
    darkMode: {
      // Override the default dark theme. Keep the manager theme unchanged
      dark: themes.blenderTheme,
      darkClass: null,
      // Override the default light theme
      light: themes.blenderTheme,
      lightClass: null,
    },
    options: {
      storySort: {
        order: [
          'Home',
          'Properties',
          'Low level',
        ],
      },
    },
  },
};

export default preview;
