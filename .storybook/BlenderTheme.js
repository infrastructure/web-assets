import { create } from '@storybook/theming';

import imgLogo from '../src/stories/assets/blender_logotype.svg';

// Blender Web Assets constants
/* Constants match BWA' root variables namings.
 */
const COLOR_ACCENT = 'hsl(204, 100%, 50%)';
const COLOR_BG_DARK = 'hsl(213, 10%, 12%)';
const COLOR_BG_PRIMARY_DARK = 'hsl(213, 10%, 21%)';
const COLOR_BG = 'hsl(213, 4%, 95%)';
const BORDER_COLOR_DARK = 'hsla(213, 10%, 80%, .15)';
const COLOR_TEXT_DARK = 'hsl(213, 10%, 80%)';
const COLOR_BG_SECONDARY_DARK = 'hsl(213, 10%, 16%)';
const INPUT_COLOR_BG_DARK = 'hsl(213, 10%, 14%)';
const INPUT_COLOR_BORDER = 'hsl(213, 10%, 14%)';
const BORDER_RADIUS = 6;

export default create({
  // Typography
  fontBase: '"Inter", sans-serif',

  brandImage: imgLogo,
  brandTitle: 'Blender Storybook',
  brandUrl: 'https://www.blender.org',

  // Colours
  colorPrimary: COLOR_ACCENT,
  colorSecondary: COLOR_ACCENT,

  // UI
  appBg: COLOR_BG_DARK,
  appBorderColor: BORDER_COLOR_DARK,
  appBorderRadius: BORDER_RADIUS,
  appContentBg: COLOR_BG_PRIMARY_DARK,
  appPreviewBg: COLOR_BG,

  // Text colors
  textColor: COLOR_TEXT_DARK,

  // Toolbar default and active colors
  barBg: COLOR_BG_SECONDARY_DARK,
  barHoverColor: COLOR_ACCENT,
  barSelectedColor: COLOR_ACCENT,
  barTextColor: COLOR_TEXT_DARK,

  // Form colors
  inputBg: INPUT_COLOR_BG_DARK,
  inputBorder: INPUT_COLOR_BORDER,
  inputBorderRadius: BORDER_RADIUS,
  inputTextColor: COLOR_TEXT_DARK,
});
