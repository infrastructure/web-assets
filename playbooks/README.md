---
gitea: none
include_toc: true
---

# Playbooks

Make sure all submodules are checked out:

    git submodule update --init --recursive

To avoid adding more dependencies to the project itself, `ansible` uses its own `virtualenv`.
To set it up use the following commands:

    python3.10 -m venv .venv
    source .venv/bin/activate
    pip install -r shared/requirements.txt

## First time install

    ./ansible.sh -i environments/staging install.yaml --skip-tags=snippets --diff --check

    ./ansible.sh -i environments/staging shared/setup_certificate.yaml --skip-tags=error-pages --diff --check

# Deploy

1. commit and push your changes to `main`;

At this point changes can be deployed to staging:
```
./ansible.sh -i environments/staging deploy.yaml
```

2. push the same exact changes to `production` using the following:

```
git fetch origin main:production && git push origin production
```

3. navigate to the playbooks and run `shared/deploy.yaml`

```
./ansible.sh -i environments/production deploy.yaml
```

**N.B**: Deploy playbook in this repository locally builds a static website, which is then
copied to a remote host. The result of this build is not tracked by the repository.
