#!/bin/bash

# Init variables
dry_mode="false"
filename=""
search_directory="." # Default search directory is the current directory

# Process arguments
while [[ $# -gt 0 ]]; do
  case "$1" in
    -n)
      dry_mode="true"
      echo "Running in dry mode. No files will be modified."
      shift
      ;;
    -d)
      shift
      search_directory="$1" # Set the search directory
      shift
      ;;
    *)
      filename="$1"
      shift
      ;;
  esac
done

# Check if a filename was passed
if [[ -z "$filename" ]]; then
  echo "Error: Filename was not passed as an argument."
  exit 1
fi

# Read map from the specified file
declare -A map
while IFS='|' read -r key value; do
  map["$key"]="$value"
done < "$filename"

# Create function to search and replace in a file
search_and_replace() {
  local file="$1"
  local temp_file="/tmp/$$.tmp" # Temporary file to store changes
  local line_number=1

  while IFS= read -r line; do
    for key in "${!map[@]}"; do
      if [[ "$line" == *"$key"* ]]; then
        # Replace the key with the corresponding value
        line="${line//$key/${map[$key]}}"
        echo "Replaced '$key' with '${map[$key]}' in $file (line $line_number)"
      fi
    done
    echo "$line" >> "$temp_file"
    ((line_number++))
  done < "$file"

  # Replace the original file with the modified content (unless in dry mode)
  if [[ "$dry_mode" != "true" ]]; then
    mv "$temp_file" "$file"
  fi
}

# Find files and process them in the specified directory
find "$search_directory" \( -name "assets_shared" -o -name "node_modules" \) -prune -o \( -name "*.css" -o -name "*.html" -o -name "*.js" -o -name "*.php" -o -name "*.sass" -o -name "*.scss" \) -type f -print |
while read -r file; do
  search_and_replace "$file"
done

echo "Search and replace completed."
