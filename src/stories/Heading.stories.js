import { createHeading } from './Heading';

export default {
  args: {
    headerLevel: 1,
  },
  argTypes: {
    headerLevel: {
      control: {
        max: 6,
        min: 1,
        type: 'number',
      },
    },
  },
  render: (args) => createHeading(args),
  tags: ['autodocs'],
  title: 'Low level/Heading',
};

export const Default = {};

export const Heading2 = {
  args: {
    headerLevel: 2,
  },
};

export const Heading3 = {
  args: {
    headerLevel: 3,
  },
};

export const Heading4 = {
  args: {
    headerLevel: 4,
  },
};
export const Heading5 = {
  args: {
    headerLevel: 5,
  },
};
export const Heading6 = {
  args: {
    headerLevel: 6,
  },
};
