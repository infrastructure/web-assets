export const createHeading = ({
  headerLevel = 1,
}) => {
  const heading = document.createElement(`h${headerLevel}`);

  heading.innerText = `Heading ${headerLevel}`;

  return heading;
};
