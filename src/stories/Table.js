import { formatCode } from './utils/format-code';

export const createTable = ({
  isDataTable = false,
  hasHover = true,
  hasRowLink = false,
}) => {
  const table = document.createElement('table');
  const tbody = document.createElement('tbody');
  const thead = document.createElement('thead');

  const createRow = (content, hasRowLink) => {
    const row = document.createElement('tr');

    if (hasRowLink) {
      row.classList.add('table-row-link');
    }

    row.insertAdjacentHTML('beforeend', content);

    return row;
  };

  const tbodyRow1 = createRow(`
    <td>1</td>
    <td>Lorem</td>
    <td>Ipsum</td>
    <td>Value</td>
  `, hasRowLink);
  const tbodyRow2 = createRow(`
    <td>2</td>
    <td>Lorem</td>
    <td>Ipsum</td>
    <td>Value</td>
  `, hasRowLink);
  const tbodyRow3 = createRow(`
    <td>3</td>
    <td>Lorem</td>
    <td>Ipsum</td>
    <td>Value</td>
  `, hasRowLink);
  const theadRow = createRow(`
    <td>#</td>
    <td>Column 1</td>
    <td>Column 2</td>
    <td>Column 3</td>
  `, hasRowLink);

  table.className = ['table', isDataTable ? 'dataTable' : '', hasHover ? 'table-hover' : ''].filter(Boolean).join(' ');

  tbody.appendChild(tbodyRow1);
  tbody.appendChild(tbodyRow2);
  tbody.appendChild(tbodyRow3);

  thead.appendChild(theadRow);

  table.appendChild(thead);
  table.appendChild(tbody);

  const tableFormattedCode = formatCode(table.outerHTML);

  return tableFormattedCode;
};
