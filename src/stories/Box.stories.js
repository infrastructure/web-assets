import { createBox } from './Box';

export default {
  args: {
    headerLevel: 2,
  },
  argTypes: {
    headerLevel: {
      control: {
        max: 6,
        min: 2,
        type: 'number',
      },
    },
  },
  parameters: {
    docs: {
      description: {
        component: `
A Box is a container element to make content stand out by giving it a light background, padding, and shadow. It looks similar to a Card.

Box might be removed in the future, to be replaced with a single Card.
        `,
      },
    },
  },
  render: (args) => createBox(args),
  tags: ['autodocs'],
  title: 'High level/Box',
};

export const Default = {};
