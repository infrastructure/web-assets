import { formatCode } from './utils/format-code';

export const createQuote = ({
  hasCite = true,
}) => {
  const cite = document.createElement('cite');
  const citeUrl = 'blender.org';
  const paragraph = `
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Here's an example of some code:
    </p>
    <pre><code>&lt;h1&gt;Hello world!&lt;/h1&gt;</code></pre>
    <p>
      and some more text.
    </p>
  `;
  const quote = document.createElement('blockquote');

  cite.insertAdjacentHTML('beforeend', `
    — Example Source (${citeUrl})
  `);
  quote.insertAdjacentHTML('beforeend', paragraph);

  if (hasCite) {
    quote.setAttribute('cite', `https://${citeUrl}`);
    quote.appendChild(cite);
  }

  const quoteFormattedCode = formatCode(quote.outerHTML);

  return quoteFormattedCode;
};
