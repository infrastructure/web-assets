import { createNavGlobalDropdownHelper } from './NavGlobalDropdown';

export default {
  parameters: {
    docs: {
      description: {
        component: `
The NavGlobalDropdown is used across various Blender.org projects to provide consistent information and navigation.

NavGlobalDropdown is a helper story and is meant to be used within the [NavGlobal](?path=/docs/high-level-navglobal--docs) component.
        `,
      },
    },
  },
  render: (args) => createNavGlobalDropdownHelper(args),
  tags: ['autodocs'],
  title: 'High level/NavGlobalDropdown',
};

export const Default = {};
