import { createNavTabs } from './NavTabs';

export default {
  parameters: {
    docs: {
      description: {
        component: `
A NavTab renders a set of navigation tabs that allow users to switch between different views within a page.

The tab switching logic is not provided by Web Assets yet.

TODO: consider adding js logic to handle tab switching.
        `,
      },
    },
    layout: 'fullscreen',
  },
  render: (args) => createNavTabs(args),
  tags: ['autodocs'],
  title: 'High level/NavTabs',
};

export const Default = {};
