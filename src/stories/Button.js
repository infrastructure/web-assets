export const createButton = ({
  isLink = false,
  label = 'Default',
  variant,
}) => {
  let btn;

  if (isLink) {
    btn = document.createElement('a');
    btn.href = '#';
  } else {
    btn = document.createElement('button');
  }

  btn.className = ['btn', variant ? `btn-${variant}` : ''].filter(Boolean).join(' ');
  btn.innerText = label;
  btn.type = 'button';

  return btn;
};
