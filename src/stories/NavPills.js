import { formatCode } from './utils/format-code';

export const createNavPills = ({
  hasItemsAccount = false,
  hasItemsAuth = false,
  isVertical = false,
}) => {
  const navPills = document.createElement('nav');

  navPills.className = ['nav nav-pills', isVertical ? `flex-column` : ''].filter(Boolean).join(' ');
  navPills.insertAdjacentHTML('beforeend', `
    <a class="nav-pills-item active" href="#">
      <i class="i-home"></i>
      Home
    </a>
    <a class="nav-pills-item " href="#">
      <i class="i-user"></i>
      Profile
    </a>
    <a class="nav-pills-item " href="#">
      <i class="i-users"></i>
      Teams
    </a>
    <a class="nav-pills-item " href="#">
      <i class="i-lock"></i>
      Tokens
    </a>
    <div class="nav-pills-divider"></div>
    <a class="nav-pills-item " href="#">
      <i class="i-heart"></i>
      Favourites
    </a>
    <a class="nav-pills-item " href="#">
      <i class="i-layers"></i>
      My Presentations
    </a>
    <a class="nav-pills-item " href="#">
      <i class="i-tv"></i>
      My Festival Entries
    </a>
    <a class="nav-pills-item " href="#">
      <i class="i-ticket"></i>
      Tickets
    </a>
    ${hasItemsAuth ? `
      <div class="nav-pills-divider"></div>
      <a class="nav-pills-item " href="#">
        <i class="i-activity"></i>
        Sessions
      </a>
      <a class="nav-pills-item " href="#">
        <i class="i-link"></i>
        Authenticated Apps
      </a>
    ` : ''}
    ${hasItemsAccount ? `
      <div class="nav-pills-divider"></div>
      <a class="nav-pills-item " href="#">
        <i class="i-shield"></i>
        Multi-factor Authentication
      </a>
      <a class="nav-pills-item " href="#">
        <i class="i-asterisk"></i>
        Password
      </a>
      <a class="nav-pills-item " href="#">
        <i class="i-trash"></i>
        Delete Account
      </a>
    ` : ''}
  `);

  const navPillsFormattedCode = formatCode(navPills.outerHTML);

  return navPillsFormattedCode;
};
