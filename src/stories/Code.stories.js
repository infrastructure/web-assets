import { createCode } from './Code';

export default {
  args: {
    content: `function doSomething() {
  console.log("Hello, world!");
}

doSomething();`,
    variant: 'block',
  },
  argTypes: {
    variant: {
      control: { type: 'select' },
      options: ['block', 'inline'],
    },
  },
  render: (args) => createCode(args),
  tags: ['autodocs'],
  title: 'Low level/Code',
};

export const Default = {};

export const Inline = {
  args: {
    content: `console.log("Hello, world!");`,
    variant: 'inline',
  },
};
