import { formatCode } from './utils/format-code';

export const createNavbarPrimary = ({
  hasBtnCta = true,
}) => {
  const navbarPrimary = document.createElement('div');

  navbarPrimary.className = 'navbar navbar-expand-md navbar-primary';
  navbarPrimary.insertAdjacentHTML('beforeend', `
    <div class="container">
      <a class="navbar-brand navbar-logo-blender" href="https://www.blender.org" title="Go to blender.org"></a>

      <button class="navbar-mobile-toggler js-dropdown-toggle" data-toggle-menu-id="navbar-mobile">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon"><i class="i-menu"></i></span>
      </button>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-menu">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#" title="Link Title">Link Active</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#" title="Link Title">Link Inactive</a>
          </li>
          ${hasBtnCta ? `
            <li class="nav-item">
              <a class="nav-link" href="#">
                <i class="i-heart"></i>
                Donate
              </a>
           </li>
          ` : ''}
        </ul>
      </div><!-- /.navbar-collapse -->
    </div>
  `);
  navbarPrimary.setAttribute('role', 'navigation');

  const navbarPrimaryFormattedCode = formatCode(navbarPrimary.outerHTML);

  return navbarPrimaryFormattedCode;
};
