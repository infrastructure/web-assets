import { createButtons } from './Buttons';

export default {
  argTypes: {
    variant: {
      control: { type: 'select' },
      options: ['col', 'dropdown', 'group', 'group-col', 'row', 'row-fluid'],
    },
  },
  parameters: {
    docs: {
      description: {
        component: `
A Buttons component groups related buttons together. It provides various layouts such as columns, dropdowns, groups, and rows.

Using Buttons is preferred when combining multiple buttons together over generic layout utilities, for consistency.
        `,
      },
    },
  },
  render: (args) => createButtons(args),
  tags: ['autodocs'],
  title: 'High level/Buttons',
};

export const Default = {};

export const Column = {
  args: {
    variant: 'col',
  },
};

export const Dropdown = {
  args: {
    variant: 'dropdown',
  },
};

export const Group = {
  args: {
    variant: 'group',
  },
};

export const GroupColumn = {
  args: {
    variant: 'group-col',
  },
};

export const Row = {
  args: {
    variant: 'row',
  },
};

export const RowFluid = {
  args: {
    variant: 'row-fluid',
  },
};
