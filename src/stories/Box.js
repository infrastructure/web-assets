import { formatCode } from './utils/format-code';

export const createBox = ({
  headerLevel = 2,
}) => {
  const box = document.createElement('div');

  box.className = 'box';
  box.insertAdjacentHTML('beforeend', `
    <h${headerLevel}>Box Title</h${headerLevel}>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in dui mauris. Vivamus hend rerit arcu, sed faucibus risus. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.
    </p>
  `);

  const boxFormattedCode = formatCode(box.outerHTML);

  return boxFormattedCode;
};
