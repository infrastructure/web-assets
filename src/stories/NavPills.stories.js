import { createNavPills } from './NavPills';

export default {
  args: {
    hasItemsAccount: false,
    hasItemsAuth: false,
    isVertical: false,
  },
  argTypes: {
    hasItemsAccount: { control: 'boolean' },
    hasItemsAuth: { control: 'boolean' },
    isVertical: { control: 'boolean' },
  },
  parameters: {
    docs: {
      description: {
        component: `
A NavPills is a navigation component that displays a series of buttons or 'pills' in either a horizontal or vertical layout. It typically includes account- and authentication-related items commonly found in Blender.org projects.

When using NavPills with the example items in projects, follow the order of pills as shown here for consistency.
        `,
      },
    },
  },
  render: (args) => createNavPills(args),
  tags: ['autodocs'],
  title: 'High level/NavPills',
};

export const Default = {};

export const Vertical = {
  args: {
    hasItemsAccount: true,
    hasItemsAuth: true,
    isVertical: true,
  },
};
