import { createPagination } from './Pagination';

export default {
  args: {
    hasBtnFirstLast: true,
    hasBtnPrevNext: false,
    isExtended: false,
    isInline: false,
    variant: 'pageItems',
  },
  argTypes: {
    hasBtnFirstLast: { control: 'boolean' },
    hasBtnPrevNext: { control: 'boolean' },
    isExtended: { control: 'boolean' },
    isInline: { control: 'boolean' },
    variant: {
      control: { type: 'select' },
      options: ['pageItems', 'pageStats'],
    },
  },
  parameters: {
    docs: {
      description: {
        component: `
A Pagination displays a series of buttons that allow users to navigate through pages of content. The component includes optional buttons for navigating to the first and last pages, as well as the previous and next pages.

As a general rule, select the simplest variant that meets your needs. If you need to display additional information about the pagination, consider using the _'Stats'_ variant.
        `,
      },
    },
    layout: 'fullscreen',
  },
  render: (args) => createPagination(args),
  tags: ['autodocs'],
  title: 'High level/Pagination',
};

export const Default = {};

export const Extended = {
  args: {
    hasBtnPrevNext: true,
    isExtended: true,
  },
};

export const Inline = {
  args: {
    isInline: true,
  },
};

export const Stats = {
  args: {
    hasBtnPrevNext: true,
    variant: 'pageStats',
  },
};
