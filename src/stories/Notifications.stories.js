import { createNotifications } from './Notifications';

export default {
  parameters: {
    docs: {
      description: {
        component: `
A Notifications component displays a series of notifications in a list. Notifications items include a timestamp, a message, and action buttons in Blender.org projects.

The _'Mark as read'_ activation logic is not provided by Web Assets yet.
        `,
      },
    },
  },
  render: (args) => createNotifications(args),
  tags: ['autodocs'],
  title: 'High level/Notifications',
};

export const Default = {};
