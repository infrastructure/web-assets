import { formatCode } from './utils/format-code';

export const createNavbarSecondary = ({
  hasBtnCta = true,
}) => {
  const navbarSecondary = document.createElement('header');

  navbarSecondary.className = 'navbar navbar-secondary';
  navbarSecondary.insertAdjacentHTML('beforeend', `
    <div class="container">
      <ul class="navbar-nav">
        <li class="nav-item nav-parent">
          <a href="#" class="nav-link">Second Level</a>
        </li>
        <li class="nav-item nav-separator">
          <i class="i-chevron-right"></i>
        </li>
        <li class="nav-item active">
          <a href="#" class="nav-link">Link Active</a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">Link</a>
        </li>

        <!-- Dropdown. -->
        <li class="nav-item dropdown">
          <button class="dropdown-toggle js-dropdown-toggle" data-toggle-menu-id="js-nav-example-dropdown">
            Dropdown <i class="i-chevron-down"></i>
          </button>
          <ul class="dropdown-menu dropdown-menu-right js-dropdown-menu" id="js-nav-example-dropdown">
            <li>
              <a class="dropdown-item" href="#">
                <i class="i-edit"></i> My Submissions
              </a>
            </li>
            <li>
              <a class="dropdown-item" href="#">
                <i class="i-star"></i> My Favorites
              </a>
            </li>
            <li class="dropdown-divider"></li>
            <li>
              <a class="dropdown-item" href="#">
                <i class="i-log-out"></i> Log out
              </a>
            </li>
          </ul>
        </li>
        <!-- // Dropdown. -->
      </ul>

      ${hasBtnCta ? `
        <button class="js-toggle-theme-btn"><i class="js-toggle-theme-btn-icon i-adjust"></i></button>
      ` : ''}

    </div>
  `);
  navbarSecondary.setAttribute('role', 'navigation');

  const navbarSecondaryFormattedCode = formatCode(navbarSecondary.outerHTML);

  return navbarSecondaryFormattedCode;
};
