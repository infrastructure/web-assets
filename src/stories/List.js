import { formatCode } from './utils/format-code';

export const createList = ({
  isInline = false,
  isUnstyled = false,
  hasSubitem = false,
}) => {
  let content;
  const list = document.createElement('ul');

  if (hasSubitem) {
    content = `
      <li>Item One</li>
      <li>Item Two with a nested <code>ul</code>
        <ul>
          <li>Sub-item
            <ul>
              <li>Sub-sub Item</li>
            </ul>
          </li>
        </ul>
      </li>
      <li>Item Three</li>
    `;
  } else {
    content = `
      <li>Item One</li>
      <li>Item Two</li>
      <li>Item Three</li>
    `;
  }

  list.className = [
    isInline ? `list-inline` : '',
    isUnstyled ? `list-unstyled` : '',
  ].filter(Boolean).join(' ');
  list.insertAdjacentHTML('beforeend', content);

  const listFormattedCode = formatCode(list.outerHTML);

  return listFormattedCode;
};
