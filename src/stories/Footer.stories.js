import { createFooter } from './Footer';

export default {
  args: {
    hasBtnDonate: true,
  },
  argTypes: {
    hasBtnDonate: { control: 'boolean' },
  },
  parameters: {
    docs: {
      description: {
        component: `
The Footer is used across various Blender.org projects to provide consistent information and navigation. It is also bundled as a self-contained component in Web Assets, that can be included in projects that don't use BWA.
        `,
      },
    },
  },
  render: (args) => createFooter(args),
  tags: ['autodocs'],
  title: 'High level/Footer',
};

export const Default = {};
