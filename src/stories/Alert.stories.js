import { createAlert } from './Alert';

export default {
  args: {
    label: 'Default',
  },
  argTypes: {
    label: { control: 'text' },
    variant: {
      control: { type: 'select' },
      options: ['danger', 'info', 'success', 'warning'],
    },
  },
  render: (args) => createAlert(args),
  tags: ['autodocs'],
  title: 'Low level/Alert',
};

export const Default = {};

export const Danger = {
  args: {
    label: 'Danger',
    variant: 'danger',
  },
};

export const Information = {
  args: {
    label: 'Information',
    variant: 'info',
  },
};

export const Success = {
  args: {
    label: 'Success',
    variant: 'success',
  },
};

export const Warning = {
  args: {
    label: 'Warning',
    variant: 'warning',
  },
};
