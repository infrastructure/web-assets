import { createNavGlobal } from './NavGlobal';

export default {
  args: {
    hasBtnCta: true,
    hasBtnNotifications: true,
    hasBtnToggleTheme: true,
    hasSearch: true,
  },
  argTypes: {
    hasBtnCta: { control: 'boolean' },
    hasBtnNotifications: { control: 'boolean' },
    hasBtnToggleTheme:{ control: 'boolean' },
    hasSearch: { control: 'boolean' },
  },
  parameters: {
    docs: {
      description: {
        component: `
The NavGlobal is used across various Blender.org projects to provide consistent information and navigation. It is also bundled as a self-contained component in Web Assets, that can be included in projects that don't use BWA.

NavGlobal encapsulates the [NavGlobalDropdown](?path=/docs/high-level-navglobaldropdown--docs) component.
        `,
      },
    },
    layout: 'fullscreen',
  },
  render: (args) => createNavGlobal(args),
  tags: ['autodocs'],
  title: 'High level/NavGlobal',
};

export const Default = {};
