import './page.css';

import { createCard } from './Card';
import { createHero } from './Hero';
import { createFooter } from './Footer';
import { createNavbarPrimary } from './NavbarPrimary';
import { createNavbarSecondary } from './NavbarSecondary';
import { createNavGlobal } from './NavGlobal';

export const createPage = () => {
  const content = `
    ${createNavGlobal({
      hasBtnToggleTheme: false,
    })}
    ${createNavbarPrimary({})}
    ${createNavbarSecondary({
      hasBtnCta: false,
    })}
    ${createHero({})}

    <section class="py-5">
      <div class="container">
        <div class="row">
          <div class="col">
            <h2>Cards</h2>

            ${createCard({
              countTotal: 6,
            })}
          </div>
        </div>
      </div>
    </section>

    ${createFooter({})}
  `;

  return content;
};
