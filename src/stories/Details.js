import { formatCode } from './utils/format-code';
import imgExample from './assets/img-example-1.jpg';

export const createDetails = ({
  isOpen = true,
}) => {
  const detailsContent1 = `
    <summary>Collapsible Content 1</summary>
    <p>
      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf
      moon officia aute, non cupidatat skateboard dolor brunch.
    </p>
  `;
  const detailsContent2 = `<summary>Collapsible Content 2</summary>
    <ul>
      <li>List item #1</li>
      <li>List item #2</li>
      <li>List item #3</li>
    </ul>
  `;
  const detailsContent3 = `
    <summary>Collapsible Content 3</summary>
    <img alt="" src="${imgExample}">
  `
  const detailsContents = [detailsContent1, detailsContent2, detailsContent3]

  /* Storybook expects a single HTML snippet or DOM node. */
  const helperStorybook = document.createElement('div');

  detailsContents.forEach(item => {
    const details = document.createElement('details');

    if (isOpen) {
      details.setAttribute('open', '');
    }

    details.insertAdjacentHTML('beforeend', item);
    helperStorybook.appendChild(details);
  });

  /* Format code helperStorybook. */
  const helperStorybookFormattedCode = formatCode(helperStorybook.outerHTML);

  return helperStorybookFormattedCode;
};
