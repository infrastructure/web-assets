import { createHero } from './Hero';

export default {
  args: {
    hasOverlay: true,
  },
  argTypes: {
    hasOverlay: { control: 'boolean' },
  },
  parameters: {
    docs: {
      description: {
        component: `
A Hero is used as a prominent section to showcase the key content or message 'above the fold' on pages.
        `,
      },
    },
    layout: 'fullscreen',
  },
  render: (args) => createHero(args),
  tags: ['autodocs'],
  title: 'High level/Hero',
};

export const Default = {};
