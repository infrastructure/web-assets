import { formatCode } from './utils/format-code';

import imgExample1 from './assets/img-example-1.jpg';
import imgExample2 from './assets/img-example-2.jpg';
import imgExample3 from './assets/img-example-3.jpg';
import imgExample4 from './assets/img-example-4.jpg';
import imgExample5 from './assets/img-example-5.jpg';
import imgExample6 from './assets/img-example-6.jpg';
import imgExample7 from './assets/img-example-7.jpg';
import imgExample8 from './assets/img-example-8.jpg';
import imgExample9 from './assets/img-example-9.jpg';
import imgExample10 from './assets/img-example-10.jpg';

const imgExamples = [
  imgExample1,
  imgExample2,
  imgExample3,
  imgExample4,
  imgExample5,
  imgExample6,
  imgExample7,
  imgExample8,
  imgExample9,
  imgExample10,
];

export const createCardItem = ({
  imgIndex,
  hasExcerpt = true,
  hasExtra = true,
  hasImg = true,
  styleThumbnail,
  title = 'Card Title',
}) => {
  const cardContent = document.createElement('div');
  const cardItem = document.createElement('div');

  cardContent.className = 'cards-item-content';
  cardContent.insertAdjacentHTML('beforeend', `
    <div class="cards-item-thumbnail" ${styleThumbnail ? `style="${styleThumbnail}"` : ''}>
      ${hasImg ? `<img width="640" height="360" src="${imgExamples[imgIndex]}" alt="">` : ''}
    </div>
    <div class="cards-item-title">
      ${title ? title : 'Card Title'}
    </div>
    ${hasExcerpt ? `
      <div class="cards-item-excerpt">
        <p>Card description.</p>
      </div>
    ` : ''}
    ${hasExtra ? `
      <div class="cards-item-extra">
        <ul>
          <li><a href="/">Aug 3</a></li>
          <li><a href="/" title="Link Title">Author</a></li>
        </ul>
      </div>
    ` : ''}
  `);

  cardItem.appendChild(cardContent);
  cardItem.className = 'cards-item';

  return cardItem;
}

export const createCard = ({
  countPerRow = 3,
  countTotal = 3,
  hasExcerpt = true,
  hasExtra = true,
  isHorizontal = false,
  isHorizontalCircular = false,
  isTransparent = false,
}) => {
  const cards = document.createElement('div');

  cards.className = [
    'cards',
    countPerRow ? `cards-${countPerRow}` : '',
    isHorizontal ? 'card-layout-horizontal' : '',
    isHorizontalCircular ? 'card-layout-horizontal-circle-transparent' : '',
    isTransparent ? 'card-layout-card-transparent' : ''
  ].filter(Boolean).join(' ');

  for (let i = 0; i < countTotal; i++) {
    const imgIndex = i % imgExamples.length;
    const cardsItem = createCardItem({
      imgIndex,
      hasExcerpt,
      hasExtra,
      // TODO: check if hasImg is needed
      hasImg: true,
    });

    cards.appendChild(cardsItem);
  }

  const cardsFormattedCode = formatCode(cards.outerHTML);

  return cardsFormattedCode;
};
