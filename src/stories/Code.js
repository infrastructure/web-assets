import { formatCode } from './utils/format-code';

export const createCode = ({
  content = `function doSomething() {
  console.log("Hello, world!");
}

doSomething();`,
  variant = 'block',
}) => {
  const code = document.createElement('code');
  const pre = document.createElement('pre');

  code.innerText = content;

  if (variant === 'block') {
    pre.appendChild(code);

    const preFormattedCode = formatCode(pre.outerHTML);

    return preFormattedCode;
  }

  return code;
};
