export const createAlert = ({
  label = 'Default',
  variant,
}) => {
  const alert = document.createElement('div');

  alert.innerText = label;

  alert.className = ['alert',   variant ? `alert-${variant}` : ''].filter(Boolean).join(' ');

  return alert;
};
