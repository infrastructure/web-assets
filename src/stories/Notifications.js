import { formatCode } from './utils/format-code';

export const createNotifications = () => {
  const btn = document.createElement('button');
  const list = document.createElement('ul');
  const notifications = document.createElement('div');

  let index = 0;

  const createNotificationsItem = ({
    content,
    hasBtnOnHover = true,
    hasIcon = true,
    isRead,
    time,
  }) => {
    const notificationsItem = document.createElement('li');

    notificationsItem.className = ['notifications-item', isRead ? 'is-read' : ''].filter(Boolean).join(' ');
    notificationsItem.insertAdjacentHTML('beforeend', `
      <div class="notifications-item-time">
        ${time}
      </div>
      <div class="notifications-item-content">
        <a href="#">
          ${hasIcon ? '<i class="i-comment"></i>' : ''}

          <span class="me-2">${content}</span><span class="notifications-item-dot"></span></a>
        </div>

        ${hasBtnOnHover ? `
          <div class="notifications-item-nav">
            <!-- Show optional notifications item nav on hover -->
            <form class="notifications-item-nav-hover me-2">
              <button action="#" class="btn" title="Mark as read" type="submit"><i class="i-eye"></i></button>
            </form>
          </div>
        `: '' }

        <div class="dropdown">
          <button class="btn btn-link dropdown-toggle js-dropdown-toggle active" data-toggle-menu-id="js-notifications-item-nav-${index}">
          <i class="i-more-vertical"></i>
          </button>
          <ul class="dropdown-menu dropdown-menu-right js-dropdown-menu" id="js-notifications-item-nav-${index}">
            <li>
              <a class="dropdown-item nav-item-mark-as-read" href="#" title="Mark as read"><i class="i-eye"></i> Mark as read </a>
            </li>
            <li>
              <a class="dropdown-item nav-item-mark-as-unread" href="#" title="Mark as read"><i class="i-eye-off"></i> Mark as unread </a>
            </li>
          </ul>
        </div>
      </div>
    `);

    index++;

    return notificationsItem;
  }

  const notificationsItem1 = createNotificationsItem({
    content: `<span class="me-2"><strong>John Doe</strong> requested review <strong>Theme 'Awesome theme'</strong></span>`,
    time: '23 h',
  });

  const notificationsItem2 = createNotificationsItem({
    content: `<strong>John Doe</strong> requested review  <strong>Theme 'Awesome theme'</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit.`,
    isRead: true,
    time: '6 d',
  });

  const notificationsItem3 = createNotificationsItem({
    content: `<strong>John Doe</strong> requested review  <strong>Theme 'Awesome theme'</strong>`,
    isRead: true,
    time: '4 w',
  });

  btn.className = 'btn mb-3';
  btn.textContent = 'Mark all as read';

  list.appendChild(notificationsItem1);
  list.appendChild(notificationsItem2);
  list.appendChild(notificationsItem3);
  list.className = 'notifications-list';

  notifications.appendChild(btn);
  notifications.appendChild(list);
  notifications.className = 'notifications';

  const notificationsFormattedCode = formatCode(notifications.outerHTML);

  return notificationsFormattedCode;
};
