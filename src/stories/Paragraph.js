export const createParagraph = () => {
  const paragraph = document.createElement('p');

  paragraph.textContent = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget leo gravida, viverra enim sed, consectetur neque. Phasellus et odio nisi. Nullam vehicula pulvinar lorem, vel dignissim enim ornare id.`;

  return paragraph;
};
