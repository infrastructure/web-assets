import { createQuote } from './Quote';

export default {
  args: {
    hasCite: true,
  },
  argTypes: {
    hasCite: { control: 'boolean' },
  },
  render: (args) => createQuote(args),
  tags: ['autodocs'],
  title: 'Low level/Quote',
};

export const Default = {};
