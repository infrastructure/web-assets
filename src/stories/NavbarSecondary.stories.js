import { createNavbarSecondary } from './NavbarSecondary';

export default {
  args: {
    hasBtnCta: true,
  },
  argTypes: {
    hasBtnCta: { control: 'boolean' },
  },
  parameters: {
    docs: {
      description: {
        component: `
The NavbarSecondary is used across various Blender.org projects to provide consistent secondary-level information and navigation.

Pages can have multiple NavbarSecondary components if needed.
        `,
      },
    },
    layout: 'fullscreen',
  },
  render: (args) => createNavbarSecondary(args),
  tags: ['autodocs'],
  title: 'High level/NavbarSecondary',
};

export const Default = {};
