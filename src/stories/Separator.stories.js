import { createSeparator } from './Separator';

export default {
  argTypes: {
    marginY: {
      control: {
        type: 'range',
        min: 1,
        max: 5
      }
    },
  },
  render: (args) => createSeparator(args),
  tags: ['autodocs'],
  title: 'Low level/Separator',
};

export const Default = {};
