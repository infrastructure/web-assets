import { createDetails } from './Details';

export default {
  args: {
    isOpen: true,
  },
  argTypes: {
    isOpen: { control: 'boolean' },
  },
  render: (args) => createDetails(args),
  tags: ['autodocs'],
  title: 'Low level/Details',
};

export const Default = {};
