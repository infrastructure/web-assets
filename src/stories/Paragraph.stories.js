import { createParagraph } from './Paragraph';

export default {
  render: (args) => createParagraph(args),
  tags: ['autodocs'],
  title: 'Low level/Paragraph',
};

export const Default = {};
