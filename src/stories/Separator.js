export const createSeparator = ({
  marginY,
}) => {
  const separator = document.createElement('hr');

  if (marginY) {
    separator.className = `my-${marginY}`;
  }

  return separator;
};
