import { createBadge } from './Badge';

export default {
  args: {
    label: 'Default',
    isOutline: false,
    size: 'normal',
  },
  argTypes: {
    isOutline: { control: 'boolean' },
    label: { control: 'text' },
    size: {
      control: { type: 'select' },
      options: ['large', 'normal', 'small'],
    },
    variant: {
      control: { type: 'select' },
      options: ['danger', 'info', 'primary', 'secondary', 'success', 'warning'],
    },
  },
  tags: ['autodocs'],
  title: 'Low level/Badge',
  render: (args) => createBadge(args),
};

export const Default = {};

export const Danger = {
  args: {
    label: 'Danger',
    variant: 'danger',
  },
};

export const Information = {
  args: {
    label: 'Information',
    variant: 'info',
  },
};

export const Primary = {
  args: {
    label: 'Primary',
    variant: 'primary',
  },
};

export const Secondary = {
  args: {
    label: 'Secondary',
    variant: 'secondary',
  },
};

export const Success = {
  args: {
    label: 'Success',
    variant: 'success',
  },
};

export const Warning = {
  args: {
    label: 'Warning',
    variant: 'warning',
  },
};
