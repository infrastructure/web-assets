import { formatCode } from './utils/format-code';

export const createPagination = ({
  hasBtnFirstLast = true,
  hasBtnPrevNext = false,
  isExtended = false,
  isInline = false,
  variant = 'pageItems',
}) => {
  const pagination = document.createElement('ul');

  const createPaginationItemBtn = ({
    classNameCustom,
    hasChevronLeft = false,
    hasChevronRight = false,
    label,
  }) => {
    const paginationItemBtn = document.createElement('li');

    paginationItemBtn.className = `page-item ${classNameCustom}`;
    paginationItemBtn.insertAdjacentHTML('beforeend', `
      <a href="#">
        ${hasChevronLeft ? '<i class="i-chevron-left"></i>' : ''}
        ${label}
        ${hasChevronRight ? '<i class="i-chevron-right"></i>' : ''}
      </a>
    `);

    return paginationItemBtn;
  }

  const paginationBtnFirst = createPaginationItemBtn({
    classNameCustom: 'page-first',
    label: 'First',
  });

  const paginationBtnLast = createPaginationItemBtn({
    classNameCustom: 'page-last',
    label: 'Last',
  });

  const paginationBtnNext = createPaginationItemBtn({
    classNameCustom: 'page-next',
    hasChevronRight: true,
    label: 'Next',
  });

  const paginationBtnPrev = createPaginationItemBtn({
    classNameCustom: 'page-prev',
    hasChevronLeft: true,
    label: 'Previous',
  });

  let paginationPageItems;

  if (variant === 'pageItems') {
    if (isExtended === true) {
      paginationPageItems = `
        <li class="page-item">
          <a href="#">1</a>
        </li>
        <li class="page-item">
          <a href="#">2</a>
        </li>
        <li class="page-item disabled">
          <span class="page-link px-0">...</span>
        </li>
        <li class="page-item">
          <a href="#">6</a>
        </li>
        <li class="page-item">
          <a href="#">7</a>
        </li>
        <li class="page-item">
          <a href="#">8</a>
        </li>
        <li class="page-item active">
          <a href="#">9</a>
        </li>
        <li class="page-item">
          <a href="#">10</a>
        </li>
        <li class="page-item">
          <a href="#">11</a>
        </li>
        <li class="page-item">
          <a href="#">12</a>
        </li>
        <li class="page-item disabled">
          <span class="page-link px-0">...</span>
        </li>
        <li class="page-item">
          <a href="#">15</a>
        </li>
        <li class="page-item">
          <a href="#">16</a>
        </li>
      `;
    } else if (isInline === true) {
      paginationPageItems = `
        <li class="page-item active">
          <a href="#">1</a>
        </li>
        <li class="page-item">
          <a href="#">2</a>
        </li>
      `;
    } else {
      paginationPageItems = `
        <li class="page-item">
          <a href="#">41</a>
        </li>
        <li class="page-item active">
          <a href="#">42</a>
        </li>
        <li class="page-item">
          <a href="#">43</a>
        </li>
      `;
    }
  } else {
    paginationPageItems = `
      <li class="page-item page-current">
        <a href="#">Page 42 of 128</a>
      </li>
    `;
  }

  pagination.className = 'pagination';
  pagination.insertAdjacentHTML('beforeend', paginationPageItems);

  if (hasBtnPrevNext) {
    pagination.prepend(paginationBtnPrev);
    pagination.append(paginationBtnNext);
  }

  if (hasBtnFirstLast) {
    pagination.prepend(paginationBtnFirst);
    pagination.append(paginationBtnLast);
  }

  const paginationFormattedCode = formatCode(pagination.outerHTML);

  return paginationFormattedCode;
};
