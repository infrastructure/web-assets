import beautify from 'js-beautify';

export const formatCode = (string) => {
  function preFormatCode(string) {
    let result = '';
    let remaining = string;

    while (true) {
      const divIndex = remaining.indexOf('<div');
      const inputIndex = remaining.indexOf('<input');
      const labelIndex = remaining.indexOf('<label');

      let tagIndex = -1;
      let tagName = '';

      if (divIndex !== -1 && inputIndex !== -1 && labelIndex !== -1) {
        tagIndex = Math.min(inputIndex, labelIndex, divIndex);
        if (tagIndex === inputIndex) {
          tagName = 'input';
        } else if (tagIndex === labelIndex) {
          tagName = 'label';
        } else {
          tagName = 'div';
        }
      } else if (divIndex !== -1 && inputIndex !== -1) {
          tagIndex = Math.min(divIndex, inputIndex);
          tagName = tagIndex === inputIndex ? 'input' : 'div';
      } else if (divIndex !== -1 && labelIndex !== -1) {
          tagIndex = Math.min(labelIndex, divIndex);
          tagName = tagIndex === labelIndex ? 'label' : 'div';
      } else if (inputIndex !== -1 && labelIndex !== -1) {
        tagIndex = Math.min(inputIndex, labelIndex);
        tagName = tagIndex === inputIndex ? 'input' : 'label';
      } else if (divIndex !== -1) {
          tagIndex = divIndex;
          tagName = 'div';
      } else if (inputIndex !== -1) {
        tagIndex = inputIndex;
        tagName = 'input';
      } else if (labelIndex !== -1) {
        tagIndex = labelIndex;
        tagName = 'label';
      }

      if (tagIndex === -1) {
        result += remaining;
        break;
      }

      result += remaining.substring(0, tagIndex);
      remaining = remaining.substring(tagIndex);

      const endIndex = remaining.indexOf('>');
      if (endIndex === -1) {
        result += remaining;
        break;
      }

      result += '\n' + remaining.substring(0, endIndex + 1);
      remaining = remaining.substring(endIndex + 1);
    }

    return result;
  }

  const preFormattedCode = preFormatCode(string);
  const formattedCode = beautify.html(preFormattedCode, {
    indent_size: 2,
    preserve_newlines: false,
  });

  return formattedCode;
};
