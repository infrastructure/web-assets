import { createTable } from './Table';

export default {
  args: {
    isDataTable: false,
    hasHover: true,
    hasRowLink: false,
  },
  argTypes: {
    isDataTable: { control: 'boolean' },
    hasHover: { control: 'boolean' },
    hasRowLink: { control: 'boolean' },
  },
  render: (argTypes) => createTable(argTypes),
  tags: ['autodocs'],
  title: 'Low level/Table',
};

export const Default = {};

export const DataTable = {
  args: {
    isDataTable: true,
  }
};
