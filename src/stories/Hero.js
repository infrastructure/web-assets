import { formatCode } from './utils/format-code';

import imgExample8 from './assets/img-example-8.jpg';

export const createHero = ({
  hasOverlay = true,
}) => {
  const hero = document.createElement('div');

  hero.className = 'hero';
  hero.insertAdjacentHTML('beforeend', `
    <div class="container">
      <div class="hero-content">
        <h1>Web Assets</h1>
        <div class="hero-subtitle">Style and layout guidelines for blender.org projects.</div>
        <div class="hero-cta-container">
          <div class="btn-row">
            <a href="#" class="btn btn-accent">
              <span>Primary</span>
            </a>
            <a href="#" class="btn btn-link">
              <span>Secondary</span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="hero-bg" style="background-image: url('${imgExample8}'); background-position-y: 50%">
    </div>
    ${hasOverlay ? '<div class="hero-overlay"></div>' : ''}
    <div class="hero-credits">Artwork by the Blender Studio</div>
  `);

  const heroFormattedCode = formatCode(hero.outerHTML);

  return heroFormattedCode;
};
