import { createButton } from './Button';

export default {
  args: {
    isLink: false,
    label: 'Default',
  },
  argTypes: {
    isLink: { control: 'boolean' },
    label: { control: 'text' },
    variant: {
      control: { type: 'select' },
      options: ['accent', 'admin', 'danger', 'primary', 'secondary', 'success', 'warning'],
    },
  },
  render: (args) => createButton(args),
  tags: ['autodocs'],
  title: 'Low level/Button',
};

export const Default = {};

export const Accent = {
  args: {
    variant: 'accent',
  },
};

export const Admin = {
  args: {
    variant: 'admin',
  },
};

export const Danger = {
  args: {
    variant: 'danger',
  },
};

export const Primary = {
  args: {
    variant: 'primary',
  },
};

export const Secondary = {
  args: {
    variant: 'secondary',
  },
};

export const Success = {
  args: {
    variant: 'success',
  },
};

export const Warning = {
  args: {
    variant: 'warning',
  },
};
