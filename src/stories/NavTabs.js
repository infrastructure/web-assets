import { formatCode } from './utils/format-code';

export const createNavTabs = () => {
  const navTabs = document.createElement('div');

  navTabs.className = 'tabs';
  navTabs.insertAdjacentHTML('beforeend', `
    <div class="tabs">
      <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" href="#tab-1">Tab 1</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#tab-2">Tab 2</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#tab-3">Tab 3</a>
        </li>
      </ul>
      <div class="tab-content">
        <div id="tab-1" class="tab-pane active">
          <p class="text-center">Tab 1 content</p>
        </div>
        <div id="tab-2" class="tab-pane">
          <p class="text-center">Tab 2 content</p>
        </div>
        <div id="tab-3" class="tab-pane">
          <p class="text-center">Tab 3 content</p>
        </div>
      </div>
    </div>
  `);

  const navTabsFormattedCode = formatCode(navTabs.outerHTML);

  return navTabsFormattedCode;
};
