import { createCard } from './Card';

export default {
  args: {
    hasExcerpt: true,
    hasExtra: true,
    countPerRow: 3,
    countTotal: 3,
    isHorizontal: false,
    isHorizontalCircular: false,
    isTransparent: false,
  },
  argTypes: {
    hasExcerpt: { control: 'boolean' },
    hasExtra: { control: 'boolean' },
    countPerRow: {
      control: {
        type: 'range',
        min: 1,
        max: 4
      }
    },
    countTotal: {
      control: {
        type: 'range',
        min: 1,
        max: 12
      }
    },
    isHorizontal: { control: 'boolean' },
    isHorizontalCircular: { control: 'boolean' },
    isTransparent: { control: 'boolean' },
  },
  layout: 'fullscreen',
  parameters: {
    docs: {
      description: {
        component: `
Cards contain a list of cards items. A card item represents a single concept or object, such as an article or a project, and encapsulates related actions and information.


By default Cards contains three items per row. This can be customized by setting the variable \`--cards-items-per-row\` or by appending the classes \`cards-1\`, \`cards-2\`, or \`cards-4\`.

Currently not all Cards variants are cross-compatible. The function \`createCardItem\` is available for use in custom containers of other stories.
        `,
      },
    },
    options: {
      panelPosition: 'right',
    },
  },
  render: (argTypes) => createCard(argTypes),
  title: 'High level/Card',
  tags: ['autodocs'],
};

export const Default = {};

export const Transparent = {
  args: {
    isTransparent: true,
  },
};

export const Horizontal = {
  args: {
    hasExcerpt: false,
    hasExtra: false,
    isHorizontal: true,
  },
};

export const HorizontalCircular = {
  args: {
    hasExcerpt: false,
    hasExtra: false,
    isHorizontalCircular: true,
  },
};
