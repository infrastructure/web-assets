import { createList } from './List';

export default {
  args: {
    isInline: false,
    isUnstyled: false,
    hasSubitem: false,
  },
  argTypes: {
    isInline: { control: 'boolean' },
    isUnstyled: { control: 'boolean' },
    hasSubitem: { control: 'boolean' },
  },
  render: (args) => createList(args),
  tags: ['autodocs'],
  title: 'Low level/List',
};

export const Default = {};

export const Inline = {
  args: {
    isInline: true,
  },
};

export const Unstyled = {
  args: {
    isUnstyled: true,
  },
};

export const Nested = {
  args: {
    hasSubitem: true,
  },
};
