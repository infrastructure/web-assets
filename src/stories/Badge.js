export const createBadge = ({
  label = 'Default',
  isOutline = false,
  size = 'normal',
  variant,
}) => {
  const badge = document.createElement('span');
  const mode = isOutline ? 'badge-outline' : '';
  const sizeClass = size == 'large' ? 'fs-lg' : size == 'normal' ? 'fs-base' : size == 'small' ? 'fs-xs' : '';

  badge.className = ['badge', variant ? `badge-${variant}` : '', mode, sizeClass].filter(Boolean).join(' ');
  badge.innerText = label;

  return badge;
};
