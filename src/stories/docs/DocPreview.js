import '../../styles/main.sass';

export const createDocPreview = ({
  content,
}) => {
  const docPreview = document.createElement('div');

  docPreview.innerHTML = content;

  return docPreview;
};
