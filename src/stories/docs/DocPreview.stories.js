import { createDocPreview } from './DocPreview';
import { createHeading } from '../Heading';

const sampleTextCustom = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';

export default {
  args: {
    content: `
      <p>This is a placeholder preview for the following story: <code>DocPreview</code>.</p>
    `,
  },
  render: (args) => createDocPreview(args),
  title: './Helpers/DocPreview',
};

export const Default = {};

export const SpacingHorizontal = {
  args: {
    content: `
    <table class="dataTable">
      <thead>
        <tr>
          <th>Custom property</th>
          <th>Value</th>
          <th class="w-100"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <code>--spacer</code>
          </td>
          <td>
            <span>1.6rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer); width: var(--spacer);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-1</code>
          </td>
          <td>
            <span>.4rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer); width: var(--spacer-1);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-2</code>
          </td>
          <td>
            <span>.8rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer); width: var(--spacer-2);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-3</code>
          </td>
          <td>
            <span>1.6rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer); width: var(--spacer-3);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-4</code>
          </td>
          <td>
            <span>2.4rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer); width: var(--spacer-4);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-5</code>
          </td>
          <td>
            <span>4.8rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer); width: var(--spacer-5);"></div>
          </td>
        </tr>
      </tbody>
    </table>
    `,
  },
}

export const SpacingVertical = {
  args: {
    content: `
    <table class="dataTable">
      <thead>
        <tr>
          <th>Custom property</th>
          <th>Value</th>
          <th class="w-100"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <code>--spacer</code>
          </td>
          <td>
            <span>1.6rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-1</code>
          </td>
          <td>
            <span>.4rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer-1);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-2</code>
          </td>
          <td>
            <span>.8rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer-2);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-3</code>
          </td>
          <td>
            <span>1.6rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer-3);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-4</code>
          </td>
          <td>
            <span>2.4rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer-4);"></div>
          </td>
        </tr>
        <tr>
          <td>
            <code>--spacer-5</code>
          </td>
          <td>
            <span>4.8rem</span>
          </td>
          <td>
            <div style="background-color: var(--color-accent); height: var(--spacer-5);"></div>
          </td>
        </tr>
      </tbody>
    </table>
    `,
  },
}

export const TypeColour = {
  args: {
    content: `
      <ul>
        <li class="text-primary">text-primary</li>
        <li class="text-secondary">text-secondary / text-muted</li>
        <li class="text-info">text-info</li>
        <li class="text-warning">text-warning</li>
        <li class="text-danger">text-danger</li>
        <li class="text-success">text-success</li>
      </ul>
    `,
  },
};

export const TypeFamilyBody = {
  args: {
    content: `
      <p>
        ${sampleTextCustom}
      </p>
    `,
  },
}

export const TypeFamilyMonospace = {
  args: {
    content: `
      <p class="font-monospace">
        ${sampleTextCustom}
      </p>
    `,
  },
}

export const TypeHeadings = {
  args: {
    content: `
      ${createHeading({}).outerHTML}
      ${createHeading({
        headerLevel: 2,
      }).outerHTML}
      ${createHeading({
        headerLevel: 3,
      }).outerHTML}
      ${createHeading({
        headerLevel: 4,
      }).outerHTML}
      ${createHeading({
        headerLevel: 5,
      }).outerHTML}
      ${createHeading({
        headerLevel: 6,
      }).outerHTML}
    `,
  },
}

export const TypeStyle = {
  args: {
    content: `
      <ul>
        <li><code>fst-italic</code>: <span class="fst-italic">Italic</span> style.</li>
        <li><code>fst-normal</code>: <span class="fst-normal">Normal</span> style.</li>
      </ul>
    `,
  },
}

export const TypeWeight = {
  args: {
    content: `
      <ul>
        <li><code>fw-bold</code> class or <code>strong</code> HTML tag: <span class="fw-bold">Bold</span> weight.</li>
        <li><code>fw-bolder</code>: <span class="fw-bolder">Bolder</span> weight (relative to the parent element).</li>
        <li><code>fw-light</code>: <span class="fw-light">Light</span> weight.</li>
        <li><code>fw-lighter</code>: <span class="fw-lighter">Lighter</span> weight (relative to the parent element).</li>
        <li><code>fw-normal</code>: <span class="fw-normal">Normal</span>/base weight.</li>
      </ul>
    `,
  },
};
