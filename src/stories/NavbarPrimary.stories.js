import { createNavbarPrimary } from './NavbarPrimary';

export default {
  args: {
    hasBtnCta: true,
  },
  argTypes: {
    hasBtnCta: {
      control: {
        type: 'boolean',
      },
    },
  },
  parameters: {
    docs: {
      description: {
        component: `
The NavbarPrimary is used on the main Blender.org website to provide consistent information and navigation.

NavbarPrimary might be used as an alernative to [NavGlobal](/?path=/docs/high-level-navglobal--docs) in a project that is not part of the public Blender.org websites.
        `,
      },
    },
    layout: 'fullscreen',
  },
  render: (args) => createNavbarPrimary(args),
  tags: ['autodocs'],
  title: 'High level/NavbarPrimary',
};

export const Default = {};
