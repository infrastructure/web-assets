import { createPage } from './Page';

export default {
  parameters: {
    layout: 'fullscreen',
  },
  render: () => createPage(),
  title: 'Example/Page',
};

export const Page = {};
