import { createButton } from './Button';
import { formatCode } from './utils/format-code';

export const createButtons = ({
  variant,
}) => {
  const btns = document.createElement('div');

  if (variant == 'dropdown') {
    btns.className = 'btn-group';
    btns.insertAdjacentHTML('beforeend', `
      ${createButton({
      label: 'First',
      }).outerHTML}

      <div class="dropdown">
        <button class="btn btn-icon dropdown-toggle js-dropdown-toggle" data-toggle-menu-id="js-btn-group-dropdown">
          <i class="i-chevron-down"></i>
        </button>
        <ul class="dropdown-menu dropdown-menu-right js-dropdown-menu" id="js-btn-group-dropdown">
          <li>
            ${createButton({
              label: 'Second',
            }).outerHTML}
          </li>
          <li>
            ${createButton({
              label: 'Third',
            }).outerHTML}
          </li>
        </ul>
      </div>
    `);
  } else {
    if (variant) {
      btns.className = `btn-${variant}`;
    }

    btns.insertAdjacentHTML('beforeend', `
      ${createButton({
        label: 'First',
      }).outerHTML}
      ${createButton({
        label: 'Second',
      }).outerHTML}
      ${createButton({
        label: 'Third',
      }).outerHTML}
    `);
  }

  const btnsFormattedCode = formatCode(btns.outerHTML);

  return btnsFormattedCode;
};
